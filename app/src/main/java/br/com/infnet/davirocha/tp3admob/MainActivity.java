package br.com.infnet.davirocha.tp3admob;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private AdView mAdView;

    private ImageView card_a, card_b, card_c;
    private boolean showcard_a,showcard_b,showcard_c;
    private Button btnConfirm, btnTry;
    private TextView showMessage;
    private int imgCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MobileAds.initialize(this, "ca-app-pub-2413868023665754~6562975596");

        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-2413868023665754/1119077228");

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        card_a = (ImageView) findViewById(R.id.card_a);
        card_b = (ImageView) findViewById(R.id.card_b);
        card_c = (ImageView) findViewById(R.id.card_c);

        btnConfirm = (Button)findViewById(R.id.btnConfirm);
        btnTry = (Button)findViewById(R.id.btnTry);

        showMessage = (TextView) findViewById(R.id.showMessage);

        btnTry.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v){
                newGame();
            }
        });
        isCard();
        randomCards();
        cardConfirmed();
    }

    public int randomCards() {

        Random random = new Random();
        imgCard = random.nextInt(3);
        return imgCard;
    }

    public void newGame(){

        Drawable imgCard_a = getResources().getDrawable(R.drawable.card_b);
        card_a.setImageDrawable(imgCard_a);
        Drawable imgCard_b = getResources().getDrawable(R.drawable.card_b);
        card_b.setImageDrawable(imgCard_b);
        Drawable imgCard_c = getResources().getDrawable(R.drawable.card_b);
        card_c.setImageDrawable(imgCard_c);

        card_a.setEnabled(true);
        card_b.setEnabled(true);
        card_c.setEnabled(true);

        showcard_a = false;
        showcard_b = false;
        showcard_c = false;

        ViewGroup.LayoutParams p1 = card_a.getLayoutParams();
        p1.width = 250;
        card_a.setLayoutParams(p1);

        ViewGroup.LayoutParams p2 = card_b.getLayoutParams();
        p2.width = 250;
        card_b.setLayoutParams(p2);

        ViewGroup.LayoutParams p3 = card_c.getLayoutParams();
        p3.width = 250;
        card_c.setLayoutParams(p3);

        showMessage.setText("");
    }

    public void isCard(){

        card_a.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v){

                showcard_a = true;
                showcard_b = false;
                showcard_c = false;

                ViewGroup.LayoutParams p1 = card_a.getLayoutParams();
                p1.width = 350;
                card_a.setLayoutParams(p1);

                ViewGroup.LayoutParams p2 = card_b.getLayoutParams();
                p2.width = 250;
                card_b.setLayoutParams(p2);

                ViewGroup.LayoutParams p3 = card_c.getLayoutParams();
                p3.width = 250;
                card_c.setLayoutParams(p3);
            }}
        );

        card_b.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v){

                showcard_b = true;
                showcard_a = false;
                showcard_c = false;

                ViewGroup.LayoutParams p1 = card_b.getLayoutParams();
                p1.width = 350;
                card_b.setLayoutParams(p1);

                ViewGroup.LayoutParams p2 = card_a.getLayoutParams();
                p2.width = 250;
                card_a.setLayoutParams(p2);

                ViewGroup.LayoutParams p3 = card_c.getLayoutParams();
                p3.width = 250;
                card_c.setLayoutParams(p3);

            }}
        );

        card_c.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v){

                showcard_c = true;
                showcard_a = false;
                showcard_b = false;

                ViewGroup.LayoutParams p1 = card_c.getLayoutParams();
                p1.width = 350;
                card_c.setLayoutParams(p1);

                ViewGroup.LayoutParams p2 = card_a.getLayoutParams();
                p2.width = 250;
                card_a.setLayoutParams(p2);

                ViewGroup.LayoutParams p3 = card_b.getLayoutParams();
                p3.width = 250;
                card_b.setLayoutParams(p3);
            }}
        );
    }

    public void cardConfirmed(){
        btnConfirm.setOnClickListener(new Button.OnClickListener() {
                                          public void onClick(View v){

                                              randomCards();

                                              if(imgCard == 1){
                                                  showMessage.setText("Carta correta, parabens!");
                                              }
                                              else {
                                                  showMessage.setText("Carta errada. Tente novamente!");
                                              }

                                              if(showcard_a){
                                                  if(imgCard == 1){
                                                      Drawable drawable = getResources().getDrawable(R.drawable.card_a);
                                                      card_a.setImageDrawable(drawable);
                                                  }else{
                                                      Drawable drawable = getResources().getDrawable(R.drawable.card_c);
                                                      card_a.setImageDrawable(drawable);
                                                  }
                                              }else if(showcard_b){
                                                  if(imgCard == 1){
                                                      Drawable drawable = getResources().getDrawable(R.drawable.card_a);
                                                      card_b.setImageDrawable(drawable);

                                                  }else{
                                                      Drawable drawable = getResources().getDrawable(R.drawable.card_c);
                                                      card_b.setImageDrawable(drawable);
                                                  }
                                              }else if(showcard_c){
                                                  if(imgCard == 1){
                                                      Drawable drawable = getResources().getDrawable(R.drawable.card_a);
                                                      card_c.setImageDrawable(drawable);

                                                  }else{
                                                      Drawable drawable = getResources().getDrawable(R.drawable.card_c);
                                                      card_c.setImageDrawable(drawable);
                                                  }
                                              }
                                              gameOver();
                                          }
                                      }
        );
    }

    public void gameOver(){

        showcard_a = false;
        showcard_b = false;
        showcard_c = false;

        card_a.setEnabled(false);
        card_b.setEnabled(false);
        card_c.setEnabled(false);
    }
}
